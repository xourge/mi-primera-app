package udep.ing.poo.miprimeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements OnClickListener { // paso 0: implements OnClickListener
    // paso 1: agregar los elementos del layout como atributos
    private Button guardar;
    private Button cambiarContrasena;

    private EditText editNombre, editCorreo, editCargo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // paso 2: asociar los atributos a los elementos del layout
        guardar = (Button)findViewById(R.id.buttonGuardar);
        cambiarContrasena = (Button)findViewById(R.id.buttonCambiarContrasena);

        // paso 3: habilitar los atributos para que sean clickeables
        guardar.setOnClickListener(this);
        cambiarContrasena.setOnClickListener(this);

        editNombre = (EditText)findViewById(R.id.editNombre);
        editCorreo = (EditText)findViewById(R.id.editCorreo);
        editCargo = (EditText)findViewById(R.id.editCargo);
    }

    // paso 4: definir el comportamiento al hacer clic
    @Override
    public void onClick(View view) {
        if (view.getId() == cambiarContrasena.getId()) {
            // qué pasa cuando presiono el boton Cambiar contraseña
            Intent intent = new Intent(this, EditPasswordActivity.class);
            startActivity(intent);
        }

        else if (view.getId() == guardar.getId()) {

            if (!editNombre.getText().toString().equals("") && !editCorreo.getText().toString().equals("") && !editCargo.getText().toString().equals("")) {

                Intent intent = new Intent(this, MainActivity2.class);
                startActivity(intent);

            }

        }
    }
}